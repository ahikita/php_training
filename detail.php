<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>レッスン詳細</title>
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/detail.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  </head>
  <body>
    <?php readfile(dirname(__DIR__) . "/root/header.php"); ?>
    <section>
        <!--ボタン群：ユーザーによって表示切替-->
        <div class="bt_user">
          <form name="user_buttons" action="apply_conf.php" method="post">
            <button type="submit" name="apply">応募</button>
          </form>
        </div>
        <div class="bt_teacher">
            <button type="submit" name="lesson_update" class="lesson_update">更新</button>
            <button type="submit" name="lesson_detele" class="lesson_detele">削除</button>
        </div>
    </section>

    <section class="detail">
      <img class="lesson_pic" src="http://placehold.it/700x300" alt="">
      <div class="detail_contents">
        <h2>レッスンのタイトルが入るるるるるるるるる。るーるるる</h2>
        <ul class="lesson_detail_lists">
          <li class="price">
            <i class="fas fa-yen-sign"></i>
            <span>10,000円</span>
          </li>
          <li class="teacher_name">
            <i class="fas fa-user"></i>
            <span>先生の名前</span>
          </li>
          <li>
            <i class="far fa-clock"></i>
            <span>3月12日（火）12:00～14:00</span>
          </li>
          <li class="area">
            <i class="fas fa-map-marker-alt"></i>
            <span>東京</span>
          </li>
          <li class="place">
            <i class="fas fa-map-marked-alt"></i>
            <span>表参道とか？</span>
          </li>
          <li class="explanation">
            <span>説明じゃああああああああ<br />
              ほげええええええええええええええ<br />
              あんなこといいな<br />
              できたらいいな
            </span>
          </li>
        </ul>

<!--応募ボタン-->
          <div class="bt_user">
            <form name="user_buttons" action="apply_conf.php" method="post">
              <button type="submit" name="apply">応募</button>
            </form>
          </div>

        <ul class="teacher_detail_lists">
          <li class="teacher_pic">
            <img src="http://placehold.it/400x400" alt="">
          </li>
          <li class="teacher_name">
            先生の名前
          </li>
          <li class="teacher_catchcopy">
            先生のキャッチコピーなのです
          </li>
          <li class="teacher_intro">
            先生の自己紹介文。<br />
            やたらと長い経歴を語る。<br />
            実績って並べられるとうさんくさい
          </li>
        </ul>
      </div>
    </section>
    <section>
<!--応募ボタン-->
          <div class="bt_user">
            <form name="user_buttons" action="apply_conf.php" method="post">
              <button type="submit" name="apply">応募</button>
            </form>
          </div>
    </section>
  </body>
</html>
