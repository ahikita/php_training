<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>レッスン新規登録</title>
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/new.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:700" rel="stylesheet">
  </head>
  <body>
    <?php readfile(dirname(__DIR__) . "/root/header.php"); ?>
    <section>
      <form action="new_conf.php" method="post" name="login_form">
        <label id="teacher_name">
          <p>先生の名前<span class="required">必須</span></p>
          <input type="text" name="name" value="" placeholder="先生の名前">
        </label>
        <label id="teacher_catchcopy">
          <p>先生のキャッチコピー<span class="comment">30字以内で入力してください</span></p>
          <input type="text" name="catchcopy" value="" placeholder="（例）明るく前向き☆彡みなさんの幸せが私の幸せ☆彡">
        </label>
        <label id="teacher_intro">
          <p>先生の自己紹介</p>
          <textarea name="intro" rows="5" cols="80" placeholder="実績だけでなく、人柄も記入すると反応アップ！"></textarea>
        </label>
        <label id="teacher_pic">
          <p>先生の写真<span class="comment">4MBまで</span></p>
          <input type="file" name="picture01" value="">
        </label>
        <label id="title">
          <p>レッスンタイトル<span class="required">必須</span><span class="comment">30字以内で入力してください</span></p>
          <input type="text" name="title" value="" placeholder="（例）徹底3時間！マンツーマンのメイクレッスン">
        </label>
        <label id="genre">
          <p>レッスンのジャンル<span class="required">必須</span></p>
          <span class="radio_input">
            <input type="radio" name="genre" value="">メイク
          </span>
          <span class="radio_input">
            <input type="radio" name="genre" value="">ヘアスタイル
          </span>
          <span class="radio_input">
            <input type="radio" name="genre" value="">ネイル
          </span>
          <span class="radio_input">
            <input type="radio" name="genre" value="">ファッション
          </span>
          <span class="radio_input">
            <input type="radio" name="genre" value="">ボディメイク
          </span>
        </label>
        <label id="price">
          <p>料金<span class="required">必須</span><span class="comment">0円～100,000円まで</span></p>
          <input type="number" name="price" value="">
        </label>
        <label id="date">
          <p>開催日時<span class="required">必須</span></p>
          <input type="datetime-local" name="date" value="">
        </label>
        <label id="area">
          <p>開催エリア<span class="required">必須</span></p>
          <select name="area">
            <option value="">東京</option>
            <option value="">千葉</option>
            <option value="">埼玉</option>
            <option value="">神奈川</option>
          </select>
        </label>
        <label id="place">
          <p>開催場所</p>
          <input type="text" name="place" value="" placeholder="（例）新宿">
        </label>
        <label id="lesson_pic">
          <p>レッスンの写真<span class="comment">4MBまで</span></p>
          <input type="file" name="picture02" value="">
        </label>
        <label id="explanation">
          <p>レッスンの説明</p>
          <textarea name="explanation" rows="5" cols="80" placeholder="どういうレッスンを行うのか、できるだけ詳しく記入してください！"></textarea>
        </label>

        <button type="submit" name="new_conf">確認</button>
      </form>
    </section>
  </body>
</html>
