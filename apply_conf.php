<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>レッスン新規登録完了</title>
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/new_conf.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:700" rel="stylesheet">
  </head>
  <body>
    <?php readfile(dirname(__DIR__) . "/root/header.php"); ?>
    <section>
      <p>下記の入力内容を確認してください。</p>
      <dl>
        <span id="user_name">
          <dt>お名前：</dt>
          <dd>あああ</dd>
        </span>
        <span id="title">
          <dt>レッスンタイトル：</dt>
          <dd>あああ</dd>
        </span>
        <span id="price">
          <dt>料金：</dt>
          <dd>あああ円</dd>
        </span>
        <span id="date">
          <dt>開催日時：</dt>
          <dd>2月1日（月）12時～16時</dd>
        </span>
        <span id="place">
          <dt>開催場所：</dt>
          <dd>あああ</dd>
        </span>
      </dl>
        <form name="apply" action="apply_done.php" method="post">
          <button type="submit" name="apply">応募</button>
        </form>
    </section>
  </body>
</html>
