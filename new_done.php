<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>レッスン新規登録完了</title>
    <link rel="stylesheet" href="css/common.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:700" rel="stylesheet">
  </head>
  <body>
    <header>
      <h1><a href="list.php" id="logo">Plus mirai</a></h1>
      <div class="bt_header">
        <button type="button" name="logout">ログアウト</button>
      </div>
    </header>
    <section>
      <p>レッスンの新規登録が完了しました。</p>
        <a href="list.php">
          <button type="button" name="tolist">レッスン一覧へ</button>
        </a>
    </section>
  </body>
</html>
