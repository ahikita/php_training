<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>ログイン画面</title>
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/login.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  </head>
  <body>
    <header>
      <h1 id="logo">Plus mirai</h1>
    </header>
    <section>
      <form action="list.php" method="post" name="login_form">
        <div class="input_area">
          <i class="far fa-envelope"></i>
          <input type="text" name="mailaddress" value="" placeholder="メールアドレス">
        </div>
        <div class="input_area">
          <i class="fas fa-lock"></i>
          <input type="password" name="password" value="" placeholder="パスワード">
        </div>
        <button type="submit" name="login_button">ログイン</button>
      </form>
    </section>
  </body>
</html>
