<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>一覧画面</title>
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/list.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  </head>
  <body>
    
    <section>
      <div class="search_area">
        <p>キーワードからレッスンを探す</p>
        <form name="search" action="" method="get">
          <div class="search_inputs_area">
            <input type="text" name="search_keyword" value="" placeholder="レッスンを検索">
            <input type="submit" name="search_button" class="fas" value="&#xf002;">
          </div>
        </form>
      </div>
    </section>
    <section>
      <a href="new.php">
        <button type="button" name="lesson_new" class="lesson_new">レッスンの新規登録</button>
      </a>
      <div class="search_list">
        <a href="detail.php">
          <div class="list_left">
            <img src="http://placehold.it/440x300" alt="">
            <span class="genre">メイク</span>
            <span class="date">3月12日 14:00～16:00</span>
            <span class="price">10,000円</span>
          </div>
          <div class="list_right">
            <span class="title">あなたに似合うメイクを教えます！マンツーマンで徹底レクチャー！</span>
            <span class="area fas">東京</span>
            <span class="teacher_name">講師：伊集院雅</span>
            <span class="teacher_catchcopy">某大手化粧品メーカーのトップ販売員として10年以上のキャリア</span>
          </div>

          <!--ボタン群：ユーザーによって表示切替-->
            <div class="bt_user">
              <form name="user_buttons" action="apply_conf.php" method="post">
                <button type="submit" name="apply">応募</button>
              </form>
            </div>
            <div class="bt_teacher">
                <button type="submit" name="lesson_update" class="lesson_update">更新</button>
                <button type="submit" name="lesson_detele" class="lesson_detele">削除</button>
            </div>
        </a>
      </div>
    </section>
  </body>
</html>
