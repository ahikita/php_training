<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <title>レッスン新規登録完了</title>
    <link rel="stylesheet" href="css/common.css">
    <link rel="stylesheet" href="css/new_conf.css">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:700" rel="stylesheet">
  </head>
  <body>
    <?php readfile(dirname(__DIR__) . "/root/header.php"); ?>
    <section>
      <p>下記の入力内容を確認してください。</p>
      <dl>
        <span id="teacher_name">
          <dt>先生の名前：</dt>
          <dd>あああ</dd>
        </span>
        <span id="teacher_catchcopy">
          <dt>先生のキャッチコピー：</dt>
          <dd>あああ</dd>
        </span>
        <span id="teacher_intro">
          <dt>先生の自己紹介：</dt>
          <dd>あああ</dd>
        </span>
        <span id="teacher_pic">
          <dt>先生の写真</dt>
          <dd class="teacher_img"><img src="http://placehold.it/400x400" /></dd>
        </span>
        <span id="title">
          <dt>レッスンタイトル：</dt>
          <dd>あああ</dd>
        </span>
        <span id="genre">
          <dt>レッスンのジャンル：</dt>
          <dd>あああ</dd>
        </span>
        <span id="price">
          <dt>料金：</dt>
          <dd>あああ円</dd>
        </span>
        <span id="date">
          <dt>開催日時：</dt>
          <dd>2月1日（月）12時～16時</dd>
        </span>
        <span id="area">
          <dt>開催エリア：</dt>
          <dd>あああ</dd>
        </span>
        <span id="place">
          <dt>開催場所：</dt>
          <dd>あああ</dd>
        </span>
        <span id="lesson_pic">
          <dt>レッスンの写真</dt>
          <dd><img src="http://placehold.it/700x300" /></dd>
        </span>
        <span id="explanation">
          <dt>レッスンの説明：</dt>
          <dd>あああ～ほげほげ</dd>
        </span>
      </dl>
        <form name="new_conf" action="new_done.php" method="post">
          <button type="submit" name="new_register">新規登録</button>
        </form>
        <form class="" action="new.php" method="post">
          <button type="submit" name="new_modify" class="button_modify">修正する</button>
        </form>
    </section>
  </body>
</html>
